import sys
import copy
import math
from abc import ABC, abstractmethod

class Process(object):
    def __init__(self,args):
        SQUARE = 1
        RECTANGLE = 2
        TRIANGLE = 3
        POLYGON = 4
        figure_type="unknown"
        args_count = len(args)
        if (args_count<=0):
             raise ValueError("arguments count must be positive")
        if (args_count>POLYGON):
             raise ValueError("count of the arguments can't be bigger then 4")

        if args_count == SQUARE:
            square = Square(args)           
            figure_type="квадрат"
            show_result(figure_type,square.get_perimeter,square.get_area)          
        elif args_count == RECTANGLE:
           rectangle = Rectangle(args)
           figure_type="прямоугольник"
           show_result(figure_type,rectangle.get_perimeter,rectangle.get_area)
         
        elif args_count == TRIANGLE:
            triangle = Triangle(args)
            figure_type="треугольник"
            show_result(figure_type,triangle.get_perimeter,triangle.get_area)
            pass
        elif args_count == POLYGON:
            polygon = Polygon(args)
            figure_type="полигон"
            show_result(figure_type, polygon.get_perimeter, polygon.get_area)   
            pass     
        pass

class Figure(ABC):
   @abstractmethod
   def get_sides(self):pass
   @abstractmethod
   def get_perimeter(self):pass

class Polygon(Figure):
    def __init__(self,sides): 
        self.__sides=sides
        self.__perimetr=0
        self.__area=0

    @property
    def get_sides(self):
        return self.__sides;
    @property
    def get_perimeter(self):
        self.__perimetr =self.__sides[0]+self.__sides[1]+self.__sides[2]+self.__sides[3]         
        return self.__perimetr
    @property
    def get_area(self):
        half_perimetr = (self.__perimetr)/2.0
        self.__area = math.sqrt((half_perimetr-self.__sides[0])*( half_perimetr-self.__sides[1]) * (half_perimetr-self.__sides[2])*( half_perimetr-self.__sides[3]))
        return self.__area

class Rectangle(Polygon): 
   
    def get_perimeter(self): 
        self.__perimetr =(2 * (self.get_sides[0]* self.get_sides[1]))
        return self.__perimetr
   
    def get_area(self):
        self.__area=(self.get_sides[0] * self.get_sides[1])
        return  self._Polygon__area

class Square(Rectangle):
    @property
    def get_perimeter(self):
        self.__perimetr= (4 *  self.get_sides[0])
        return self.__perimetr
    @property  
    def get_area(self):
        self.__area=(self.get_sides[0]**2)
        return self.__area

class Triangle(Square):
    @property
    def get_perimeter(self):
        self.__perimetr=self.get_sides[0]+self.get_sides[1]+self.get_sides[2]
        return self.__perimetr

    @property  
    def get_area(self):
        perimeter = self.get_perimeter
        self.__area = math.sqrt(perimeter*(perimeter-self.get_sides[0])*(perimeter-self.get_sides[1])
                           * (perimeter-self.get_sides[2]))
        return self.__area

if __name__ == "__main__":
   def show_result(figure_type, perimetr_value,area_value): 
        print('Периметр {0}а равен {1}.\n Площадь {0}а равна {2}\n'.format(figure_type,perimetr_value,area_value))
   args = sys.argv
   del args[0]
   
   def prepare_values(args):
     massive = list()
     for iter in args:
            try:
                massive.append(int(iter))
            except TypeError:
                raise ValueError("Not integer!")
     return massive

   process = Process(prepare_values(args))

   #list  = [3,4,3,4]

   #p = Polygon(list); 
   #show_result("Polygon",p.get_perimeter,p.get_area)

   #r = Rectangle(list)
   #show_result("Rectangle",r.get_perimeter(),r.get_area())

   #s = Square(list)
   #show_result("Square",s.get_perimeter,s.get_area)

   #t = Triangle(list)
   #show_result("Triangle",t.get_perimeter,t.get_area)

 
